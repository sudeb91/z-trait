import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})

export class SignupComponent {
  
  signUpForm:any;
  submitted:boolean = false;
  successMsg: boolean = false;
  errorMsg:boolean = false;
  public dataResponse:any;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.signUpForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  submitSignupForm() {
    this.submitted = true;
    if (this.signUpForm.valid) {
      this.submitted = false;

      var formData: any = new FormData();
      formData.append('postType', 'endUserSignUp');
      formData.append('registerType', 'web');
      formData.append('key', 'YhgPmv8w3A84ufcx');
      formData.append('name', this.signUpForm.get('name').value);
      formData.append('user_email', this.signUpForm.get('email').value);
      formData.append('phone_no', this.signUpForm.get('phone').value);
      formData.append('user_password', this.signUpForm.get('password').value);

      this.http.post<any>('http://www.z-trait.com/api/ApiLanding.php', formData).subscribe(data => {
        this.dataResponse = data;

        if(data.success==true) {
          this.successMsg = true;
          this.errorMsg = false;
        }
        else if(data.success==false) {
          this.successMsg = false;
          this.errorMsg = true;
        }
        this.signUpForm.reset();
      });
    }
  }
}
