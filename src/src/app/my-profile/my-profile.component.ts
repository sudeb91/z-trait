import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})

export class MyProfileComponent {

  public dataResponse:any = [{
    name: '',
    phone_no: '',
    email_id: ''
  }];

  constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) {
    if(!this.auth.isLoggedIn()) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    var formData: any = new FormData();
    formData.append('postType', 'getEndUserProfileData');
    formData.append('key', 'YhgPmv8w3A84ufcx');
    formData.append('user_id', localStorage.getItem('token'));

    this.http.post<any>('http://www.z-trait.com/api/ApiLanding.php', formData).subscribe(data => {
      
      if(data.success==true) {
        this.dataResponse = data.data;
        console.log(this.dataResponse);
      }
      else if(data.success==false) {
        this.router.navigate(['/login']);
      }
    });
  }

  logout(): void {
    this.auth.logout();
  }
}
