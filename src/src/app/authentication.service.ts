import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs";
import { Router } from '@angular/router';

@Injectable({
   providedIn: 'root'
})
export class AuthenticationService {

   isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());

   constructor(private router: Router) { }

   isLoggedIn() {
    return this.isLoginSubject.value;
   }

   private hasToken() : boolean {
    return !!localStorage.getItem('token');
   }

   login(userData:any): void {
      console.log(userData);
      this.isLoginSubject.next(true); 
      localStorage.setItem('token', userData.user_id);
   }

   logout(): void {
      this.isLoginSubject.next(false);
      localStorage.removeItem('token'); 
      this.router.navigate(['/login']);
   }
}
