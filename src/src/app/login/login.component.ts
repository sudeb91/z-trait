import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { HttpClient } from '@angular/common/http';

import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm:any;
  submitted:boolean = false;

  errorMsg:boolean = false;
  public dataResponse:any;

  constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  submitLoginForm() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.submitted = false;

      var formData: any = new FormData();
      formData.append('postType', 'endUserLogin');
      formData.append('key', 'YhgPmv8w3A84ufcx');
      formData.append('user_email', this.loginForm.get('email').value);
      formData.append('user_password', this.loginForm.get('password').value);

      this.http.post<any>('http://www.z-trait.com/api/ApiLanding.php', formData).subscribe(data => {
        this.dataResponse = data;
        
        if(data.success==true) {
          this.errorMsg = false;
          this.auth.login(this.dataResponse.data);
          this.router.navigate(['/profile']);
        }
        else if(data.success==false) {
          this.errorMsg = true;
        }
      });
    }
  }
}
